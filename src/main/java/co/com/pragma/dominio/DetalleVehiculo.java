package co.com.pragma.dominio;

import java.util.List;

import co.com.pragma.dto.VehiculoRs;
import co.com.pragma.entities.DetallePedido;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DetalleVehiculo {

	private VehiculoRs vehiculo;
	private List<DetallePedido> detallePedidos;

}
