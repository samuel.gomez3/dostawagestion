package co.com.pragma.dominio;

import java.util.List;
import java.util.Objects;

import co.com.pragma.entities.DetallePedido;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Region {

	private String codigoPostal;
	private List<DetallePedido> detallePedidos;
	private Double pesoTotal;

	public void addDetallePedido(DetallePedido detallePedido) {

		this.pesoTotal += detallePedido.getPedido().getPesoGramos();
		this.detallePedidos.add(detallePedido);

	}

	@Override
	public int hashCode() {
		return Objects.hash(codigoPostal);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;

		Region region = (Region) obj;

		return codigoPostal.equals(region.codigoPostal);
	}

}
