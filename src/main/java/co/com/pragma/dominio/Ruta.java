package co.com.pragma.dominio;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Ruta {
    private List<String> ordenEntrega;
    private int distanciasMetros;
    private List<String> instrucciones;
    private int duraccionSegundos;
}
