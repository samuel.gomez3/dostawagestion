package co.com.pragma.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VehiculoRq {

	private String placa;

	private Double capacidadGramos;

	private Boolean activo;

}
