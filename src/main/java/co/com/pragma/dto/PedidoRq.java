package co.com.pragma.dto;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PedidoRq {

	private String direccion;

	private Date fechaEntrega;

	private Double pesoGramos;
}
