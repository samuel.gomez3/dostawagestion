package co.com.pragma.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VehiculoRs {
	private Long id;
	private String placa;

	private Double capacidadGramos;
	private Double capacidadDisponibleGramos;

	private Boolean activo;

}
