package co.com.pragma.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DetallePedidoRq {

	private long pedido;

	private long vehiculo;

	private long estadoPedidos;

}
