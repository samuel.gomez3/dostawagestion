package co.com.pragma.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class RutaRs {
	
    private List<PedidoRs> ordenEntrega;
    private int distanciasMetros;
    private List<String> instrucciones;
    private int duraccionSegundos;

}
