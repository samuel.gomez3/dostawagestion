package co.com.pragma.dto;

import co.com.pragma.entities.MejorRutaCacheKey;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class MejorRutaCacheRs {

	private MejorRutaCacheKey id;

	private RutaRs json;

}
