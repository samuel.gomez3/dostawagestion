package co.com.pragma.entities;

import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;

import jakarta.persistence.Lob;
import jakarta.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "mejor_ruta_cache")
public class MejorRutaCache {

	@EmbeddedId
	private MejorRutaCacheKey id;

	@Column(name = "json")
	@Lob
	private String json;

}
