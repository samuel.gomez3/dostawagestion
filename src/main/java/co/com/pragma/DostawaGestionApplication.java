package co.com.pragma;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class DostawaGestionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DostawaGestionApplication.class, args);
	}

}
