package co.com.pragma.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.com.pragma.dto.MejorRutaCacheRs;
import co.com.pragma.dto.PedidoRq;
import co.com.pragma.dto.PedidoRs;
import co.com.pragma.dto.VehiculoRq;
import co.com.pragma.dto.VehiculoRs;
import co.com.pragma.entities.Pedido;
import co.com.pragma.entities.Vehiculo;
import co.com.pragma.exception.CapacityExceededException;
import co.com.pragma.exception.NotFoundException;
import co.com.pragma.exception.UnauthorisedException;
import co.com.pragma.services.IGestionService;
import co.com.pragma.util.JwtUtil;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class GestionController {

	private final IGestionService gestionService;

	@PostMapping("/guardarVehiculo")
	public ResponseEntity<?> guardarVehiculo(@RequestHeader("token") String token, @RequestBody VehiculoRq vehiculoRq) {
		try {
			long idRol = JwtUtil.validateToken(token).get("idRol", Long.class);
			if (idRol != 1L) {
				throw new UnauthorisedException("No está autorizado para realizar esta acción");
			}
			Vehiculo vehiculo = gestionService.guardarVehiculo(vehiculoRq);
			return new ResponseEntity<>(vehiculo, HttpStatus.OK);
		} catch (UnauthorisedException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
		} catch (Exception e) {
			return new ResponseEntity<>("Se produjo un error interno", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/actualizarVehiculo/{id}")
	public ResponseEntity<?> actualizarVehiculo(@RequestHeader("token") String token, @PathVariable Long id,
			@RequestBody VehiculoRq vehiculoRq) {
		try {
			long idRol = JwtUtil.validateToken(token).get("idRol", Long.class);
			if (idRol != 1L) {
				throw new UnauthorisedException("No está autorizado para realizar esta acción");
			}
			Vehiculo vehiculo = gestionService.actualizarVehiculo(vehiculoRq, id);
			return new ResponseEntity<>(vehiculo, HttpStatus.OK);
		} catch (UnauthorisedException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
		} catch (Exception e) {
			return new ResponseEntity<>("Se produjo un error interno", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/obtenerVehiculos")
	public ResponseEntity<?> obtenerVehiculos(@RequestHeader("token") String token) {
		try {
			long idRol = JwtUtil.validateToken(token).get("idRol", Long.class);
			if (idRol != 1L) {
				throw new UnauthorisedException("No está autorizado para realizar esta acción");
			}
			List<VehiculoRs> vehiculos = gestionService.obtenerVehiculos();
			return new ResponseEntity<>(vehiculos, HttpStatus.OK);
		} catch (UnauthorisedException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
		} catch (Exception e) {
			return new ResponseEntity<>("Se produjo un error interno", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/obtenerVehiculoPorId/{id}")
	public ResponseEntity<?> obtenerVehiculoPorId(@RequestHeader("token") String token, @PathVariable Long id) {
		try {
			long idRol = JwtUtil.validateToken(token).get("idRol", Long.class);
			if (idRol != 1L) {
				throw new UnauthorisedException("No está autorizado para realizar esta acción");
			}
			VehiculoRs vehiculo = gestionService.obtenerVehiculoPorId(id);
			return new ResponseEntity<>(vehiculo, HttpStatus.OK);
		} catch (UnauthorisedException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>("Se produjo un error interno", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/eliminarVehiculoPorId/{id}")
	public ResponseEntity<?> eliminarVehiculoPorId(@RequestHeader("token") String token, @PathVariable Long id) {
		try {
			long idRol = JwtUtil.validateToken(token).get("idRol", Long.class);
			if (idRol != 1L) {
				throw new UnauthorisedException("No está autorizado para realizar esta acción");
			}
			gestionService.eliminarVehiculoPorId(id);
			return new ResponseEntity<>("El vehículo se eliminó correctamente", HttpStatus.OK);
		} catch (UnauthorisedException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>("Se produjo un error interno", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/guardarPedido")
	public ResponseEntity<?> guardarPedido(@RequestHeader("token") String token, @RequestBody PedidoRq pedidoRq) {
		try {
			long idRol = JwtUtil.validateToken(token).get("idRol", Long.class);
			if (idRol != 1L) {
				throw new UnauthorisedException("No está autorizado para realizar esta acción");
			}
			Pedido pedido = gestionService.guardarPedido(pedidoRq);
			return new ResponseEntity<>(pedido, HttpStatus.OK);
		} catch (UnauthorisedException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
		} catch (CapacityExceededException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<>("Se produjo un error interno", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/actualizacionPedido/{id}")
	public ResponseEntity<?> actualizacionPedido(@RequestHeader("token") String token, @PathVariable Long id,
			@RequestBody PedidoRq pedidoRq) {
		try {
			long idRol = JwtUtil.validateToken(token).get("idRol", Long.class);
			if (idRol != 1L) {
				throw new UnauthorisedException("No está autorizado para realizar esta acción");
			}
			Pedido pedido = gestionService.actualizacionPedido(pedidoRq, id);
			return new ResponseEntity<>(pedido, HttpStatus.OK);
		} catch (UnauthorisedException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>("Se produjo un error interno", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/obtenerPedidos")
	public ResponseEntity<?> obtenerPedido(@RequestHeader("token") String token) {
		try {
			long idRol = JwtUtil.validateToken(token).get("idRol", Long.class);
			if (idRol != 1L) {
				throw new UnauthorisedException("No está autorizado para realizar esta acción");
			}
			List<PedidoRs> pedidos = gestionService.obtenerPedido();
			return new ResponseEntity<>(pedidos, HttpStatus.OK);
		} catch (UnauthorisedException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
		} catch (Exception e) {
			return new ResponseEntity<>("Se produjo un error interno", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/obtenerPedidoPorId/{id}")
	public ResponseEntity<?> obtenerPedidoPorId(@RequestHeader("token") String token, @PathVariable Long id) {
		try {
			long idRol = JwtUtil.validateToken(token).get("idRol", Long.class);
			if (idRol != 1L) {
				throw new UnauthorisedException("No está autorizado para realizar esta acción");
			}
			PedidoRs pedido = gestionService.obtenerPedidoPorId(id);
			return new ResponseEntity<>(pedido, HttpStatus.OK);
		} catch (UnauthorisedException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>("Se produjo un error interno", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/eliminarPedidoPorId/{id}")
	public ResponseEntity<?> eliminarPedidoPorId(@RequestHeader("token") String token, @PathVariable Long id) {
		try {
			long idRol = JwtUtil.validateToken(token).get("idRol", Long.class);
			if (idRol != 1L) {
				throw new UnauthorisedException("No está autorizado para realizar esta acción");
			}
			gestionService.eliminarPedidoPorId(id);
			return new ResponseEntity<>("Pedido eliminado exitosamente", HttpStatus.OK);
		} catch (UnauthorisedException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>("Se produjo un error interno", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/consultarMejorRuta/{idVehiculo}")
	public ResponseEntity<?> consultarMejorRuta(@RequestHeader("token") String token, @PathVariable Long idVehiculo,
			@RequestParam(value = "cache", required = false, defaultValue = "false") String cache,
			@RequestParam(value = "return", required = false, defaultValue = "false") String returng,
			@RequestParam(value = "acopio", required = false, defaultValue = "false") String acopio) {
		try {
			long idRol = JwtUtil.validateToken(token).get("idRol", Long.class);
			String placa = JwtUtil.validateToken(token).getSubject();
			if (idRol != 1L && idRol != 2L) {
				throw new UnauthorisedException("No está autorizado para realizar esta acción");
			}
			Object mejorRuta = gestionService.consultarMejorRuta(placa, cache, acopio, returng);
			return new ResponseEntity<>(mejorRuta, HttpStatus.OK);
		} catch (UnauthorisedException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>("Se produjo un error interno", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/entregarPedido/{id}")
	public ResponseEntity<?> entregarPedido(@RequestHeader("token") String token, @PathVariable Long id) {
		try {
			long idRol = JwtUtil.validateToken(token).get("idRol", Long.class);
			if (idRol != 2L) {
				throw new UnauthorisedException("No está autorizado para realizar esta acción");
			}
			gestionService.entregarPedido(id);
			return new ResponseEntity<>("Entregado", HttpStatus.OK);
		} catch (UnauthorisedException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>("Se produjo un error interno", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/direccionVehiculos")
	public ResponseEntity<?> direccionVehiculos(@RequestHeader("token") String token) {
		try {
			long idRol = JwtUtil.validateToken(token).get("idRol", Long.class);
			if (idRol != 1L) {
				throw new UnauthorisedException("No está autorizado para realizar esta acción");
			}
			List<MejorRutaCacheRs> rutas = gestionService.direccionVehiculos();
			return new ResponseEntity<>(rutas, HttpStatus.OK);
		} catch (UnauthorisedException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
		} catch (Exception e) {
			return new ResponseEntity<>("Se produjo un error interno", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
