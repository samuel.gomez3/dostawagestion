package co.com.pragma.services;


import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import co.com.pragma.dominio.DetalleVehiculo;
import co.com.pragma.dominio.Region;
import co.com.pragma.dominio.Ruta;
import co.com.pragma.dto.MejorRutaCacheRs;
import co.com.pragma.dto.PedidoRq;
import co.com.pragma.dto.PedidoRs;
import co.com.pragma.dto.RutaRs;
import co.com.pragma.dto.VehiculoRq;
import co.com.pragma.dto.VehiculoRs;
import co.com.pragma.entities.DetallePedido;
import co.com.pragma.entities.EstadoPedido;
import co.com.pragma.entities.MejorRutaCache;
import co.com.pragma.entities.MejorRutaCacheKey;
import co.com.pragma.entities.Pedido;
import co.com.pragma.entities.Vehiculo;
import co.com.pragma.exception.CapacityExceededException;
import co.com.pragma.exception.NotFoundException;
import co.com.pragma.repositories.DetallePedidoRepository;
import co.com.pragma.repositories.EstadoPedidoRepository;
import co.com.pragma.repositories.MejorRutaCacheRepository;
import co.com.pragma.repositories.PedidoRepository;
import co.com.pragma.repositories.VehiculoRepository;
import co.com.pragma.util.Constants;
import co.com.pragma.util.GestionUtil;
import lombok.RequiredArgsConstructor;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.sql.Date;
import java.util.HashMap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
@RequiredArgsConstructor
public class GestionServiceImpl implements IGestionService {

	private static final Long ID_ESTADO_PEDIDO_EN_ACOPIO = 1L;
	private static final int INDICE_ESTADO_DESPACHADO = 1;
	private final PedidoRepository pedidoRepository;
	private final VehiculoRepository vehiculoRepository;
	private final DetallePedidoRepository detallePedidoRepository;
	private final EstadoPedidoRepository estadoPedidoRepository;
	private final MejorRutaCacheRepository mejorRutaCacheRepository;

	public Vehiculo guardarVehiculo(VehiculoRq vehiculoRq) throws Exception {
		return vehiculoRepository
				.save(new Vehiculo(0L, vehiculoRq.getPlaca(), vehiculoRq.getCapacidadGramos(), vehiculoRq.getActivo()));
	}

	public Vehiculo actualizarVehiculo(VehiculoRq vehiculoRq, long id) throws Exception {
		Optional<Vehiculo> vehiculoExistente = vehiculoRepository.findById(id);
		if (!vehiculoExistente.isPresent()) {
			throw new NotFoundException("Vehículo no encontrado");
		}
		Vehiculo vehiculo = new Vehiculo(id, vehiculoRq.getPlaca(), vehiculoRq.getCapacidadGramos(),
				vehiculoRq.getActivo());
		return vehiculoRepository.save(vehiculo);
	}

	public List<VehiculoRs> obtenerVehiculos() {
		return vehiculoRepository.findAll().stream().map(vehiculo -> {
			double sumatoriaPesos = detallePedidoRepository.findByVehiculo(vehiculo).stream()
					.filter(detallePedido -> detallePedido.getEstadoPedido().getId() == 2L)
					.mapToDouble(detallePedido -> detallePedido.getPedido().getPesoGramos()).sum();
			return new VehiculoRs(vehiculo.getId(), vehiculo.getPlaca(), vehiculo.getCapacidadGramos(),
					vehiculo.getCapacidadGramos() - sumatoriaPesos, vehiculo.getActivo());
		}).collect(Collectors.toList());
	}

	public VehiculoRs obtenerVehiculoPorId(long id) throws Exception {
		Optional<Vehiculo> vehiculoOptional = vehiculoRepository.findById(id);
		if (!vehiculoOptional.isPresent()) {
			throw new NotFoundException("Vehículo no encontrado");
		}
		Vehiculo vehiculo = vehiculoOptional.get();
		double sumatoriaPesos = detallePedidoRepository.findByVehiculo(vehiculo).stream()
				.filter(detallePedido -> detallePedido.getEstadoPedido().getId() == 2L)
				.mapToDouble(detallePedido -> detallePedido.getPedido().getPesoGramos()).sum();
		return new VehiculoRs(vehiculo.getId(), vehiculo.getPlaca(), vehiculo.getCapacidadGramos(),
				vehiculo.getCapacidadGramos() - sumatoriaPesos, vehiculo.getActivo());
	}

	public void eliminarVehiculoPorId(long id) throws Exception {
		Optional<Vehiculo> vehiculo = vehiculoRepository.findById(id);
		if (!vehiculo.isPresent()) {
			throw new NotFoundException("Vehículo no encontrado");
		}
		vehiculoRepository.delete(vehiculo.get());
	}

	public Pedido guardarPedido(PedidoRq pedidoRq) throws Exception {
		String direccion = pedidoRq.getDireccion();
		Pedido pedido = pedidoRepository.save(new Pedido(0L, direccion, GestionUtil.obtenerCodigoPostal(direccion),
				Date.valueOf(pedidoRq.getFechaEntrega().toLocalDate().plusDays(4)), pedidoRq.getPesoGramos()));

		Double DoumaxCapacidadDisponible = obtenerVehiculosDisponibles().stream()
				.map(VehiculoRs::getCapacidadDisponibleGramos).max(Comparator.naturalOrder())
				.orElseThrow(() -> new Exception("No hay vehículos disponibles"));

		if (pedidoRq.getPesoGramos() > DoumaxCapacidadDisponible) {
			throw new CapacityExceededException("Peso máximo excedido");
		}

		detallePedidoRepository.save(new DetallePedido(0L, pedido, null, estadoPedidoRepository.findById(1L).get()));

		return pedido;
	}

	public Pedido actualizacionPedido(PedidoRq pedidoRq, long id) throws Exception {
		Optional<Pedido> pedidoOptional = pedidoRepository.findById(id);
		if (!pedidoOptional.isPresent()) {
			throw new NotFoundException("Pedido no encontrado");
		}
		String direccion = pedidoRq.getDireccion();
		return pedidoRepository.save(new Pedido(id, direccion, GestionUtil.obtenerCodigoPostal(direccion),
				pedidoRq.getFechaEntrega(), pedidoRq.getPesoGramos()));
	}

	public List<PedidoRs> obtenerPedido() throws Exception {
		List<Pedido> pedidos = pedidoRepository.findAll();
		List<PedidoRs> pedidoRs = pedidos.stream().map(pedido -> {
			DetallePedido detallePedido = detallePedidoRepository.findByPedido(pedido).stream().findFirst()
					.orElseThrow(() -> new NotFoundException("Detalle del pedido no encontrado"));
			return new PedidoRs(pedido.getId(), pedido.getDireccion(), new Date(pedido.getFechaEntrega().getTime()),
					pedido.getPesoGramos(), detallePedido.getEstadoPedido().getNombre());
		}).collect(Collectors.toList());
		return pedidoRs;
	}

	public PedidoRs obtenerPedidoPorId(long id) throws Exception {
		Pedido pedido = pedidoRepository.findById(id).orElseThrow(() -> new NotFoundException("Pedido no encontrado"));
		DetallePedido detallePedido = detallePedidoRepository.findByPedido(pedido).stream().findFirst()
				.orElseThrow(() -> new NotFoundException("Detalle del pedido no encontrado"));
		return new PedidoRs(pedido.getId(), pedido.getDireccion(), new Date(pedido.getFechaEntrega().getTime()),
				pedido.getPesoGramos(), detallePedido.getEstadoPedido().getNombre());
	}

	public void eliminarPedidoPorId(long id) throws Exception {
		Pedido pedido = pedidoRepository.findById(id).orElseThrow(() -> new NotFoundException("Pedido no encontrado"));
		pedidoRepository.delete(pedido);
	}

	public RutaRs consultarMejorRuta(String placa, String cache, String acopio, String returng)
			throws IOException, InterruptedException, URISyntaxException {
		Vehiculo vehiculo = buscarVehiculo(placa);
		Boolean usarCache = Boolean.parseBoolean(cache);

		MejorRutaCacheKey cacheKey = generarCacheKey(vehiculo.getId());

		RutaRs rutaRs = null;
		if (usarCache) {
			Optional<MejorRutaCache> mejorRutasCache = mejorRutaCacheRepository.findById(cacheKey);
			if (mejorRutasCache.isPresent()) {
				rutaRs = obtenerRutaDeCache(mejorRutasCache.get());
			}
		}

		if (rutaRs == null) {
			rutaRs = calcularRuta(vehiculo, acopio, returng);
			guardarRutaEnCache(rutaRs, cacheKey);
		}

		return rutaRs;
	}

	public void entregarPedido(long idPedido) {
		Pedido pedido = buscarPedido(idPedido);
		DetallePedido detallePedido = buscarDetallePedido(pedido);
		EstadoPedido estadoEntregado = buscarEstadoEntregado();
		actualizarEstadoPedido(detallePedido, estadoEntregado);
	}

	public List<MejorRutaCacheRs> direccionVehiculos() {
	    java.util.Date fechaEntrega = getInicioDia();
	    List<MejorRutaCache> mejorRutaCaches = mejorRutaCacheRepository.findByIdFechaEntrega(fechaEntrega);
	    return mejorRutaCaches.stream().map(this::crearMejorRutaCacheRs).collect(Collectors.toList());
	}

	private java.util.Date getInicioDia() {
	    Calendar calendar = Calendar.getInstance();
	    calendar.set(Calendar.HOUR_OF_DAY, 0);
	    calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    calendar.set(Calendar.MILLISECOND, 0);
	    return calendar.getTime();
	}

	private MejorRutaCacheRs crearMejorRutaCacheRs(MejorRutaCache mejorRutaCache) {
	    try {
	        ObjectMapper objectMapperJson = new ObjectMapper();
	        RutaRs rutaRs = objectMapperJson.readValue(mejorRutaCache.getJson(), RutaRs.class);
	        return new MejorRutaCacheRs(mejorRutaCache.getId(), rutaRs);
	    } catch (Exception e) {
	        throw new IllegalStateException("Error al parsear la ruta", e);
	    }
	}


//	@Scheduled(cron = "0 0 8 * * MON-SAT")
//	@Scheduled(cron = "0 */10 * * * *")
//	@Scheduled(cron = "0 * * * * SAT")
	public void cargarCamiones() {
		List<EstadoPedido> estadoPedidos = estadoPedidoRepository.findAll();
		manejarPedidosConPrioridad(estadoPedidos);
		manejarPedidosSinPrioridad(estadoPedidos);
	}

//	@Scheduled(cron = "0 0 20 * * MON-SAT")
	@Scheduled(cron = "0 * * * * MON")
	public void descargarCamiones() {
		List<EstadoPedido> estadoPedidos = estadoPedidoRepository.findAll();
		List<DetallePedido> detallePedidos = detallePedidoRepository.findByEstadoPedido(estadoPedidos.get(1));
		for (DetallePedido detallePedido : detallePedidos) {
			detallePedido.setEstadoPedido(estadoPedidos.get(0));
		}
	}

	private Pedido buscarPedido(long idPedido) {
		Optional<Pedido> pedidoOpt = pedidoRepository.findById(idPedido);
		if (!pedidoOpt.isPresent()) {
			throw new NotFoundException("Pedido no encontrado con id: " + idPedido);
		}
		return pedidoOpt.get();
	}

	private DetallePedido buscarDetallePedido(Pedido pedido) {
		List<DetallePedido> detallePedidos = detallePedidoRepository.findByPedido(pedido);
		if (detallePedidos.isEmpty()) {
			throw new NotFoundException("Detalle de pedido no encontrado para el pedido con id: " + pedido.getId());
		}
		return detallePedidos.get(0);
	}

	private EstadoPedido buscarEstadoEntregado() {
		List<EstadoPedido> estadoPedidos = estadoPedidoRepository.findAll();
		if (estadoPedidos.size() < 3) {
			throw new IllegalStateException("No se encontró el estado de pedido entregado");
		}
		return estadoPedidos.get(2);
	}

	private void actualizarEstadoPedido(DetallePedido detallePedido, EstadoPedido estadoEntregado) {
		detallePedido.setEstadoPedido(estadoEntregado);
		detallePedidoRepository.save(detallePedido);
	}

	private Vehiculo buscarVehiculo(String placa) {
		List<Vehiculo> vehiculos = vehiculoRepository.findByPlaca(placa);
		if (vehiculos.isEmpty()) {
			throw new IllegalArgumentException("No se pudo encontrar el vehículo con placa: " + placa);
		}
		return vehiculos.get(0);
	}

	private MejorRutaCacheKey generarCacheKey(Long vehiculoId) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		return new MejorRutaCacheKey(vehiculoId, calendar.getTime());
	}

	private RutaRs obtenerRutaDeCache(MejorRutaCache mejorRutaCache) throws IOException {
		String json = mejorRutaCache.getJson();
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(json, RutaRs.class);
	}

	private RutaRs calcularRuta(Vehiculo vehiculo, String acopio, String returng)
			throws IOException, InterruptedException, URISyntaxException {
		List<Ruta> rutas = obtenerRutasDesdeAPI(vehiculo, acopio, returng);
		Ruta ruta = rutas.get(0);
		List<PedidoRs> pedidosOrdenadosEntrega = generarPedidosOrdenadosEntrega(ruta);
		return new RutaRs(pedidosOrdenadosEntrega, ruta.getDistanciasMetros(), ruta.getInstrucciones(),
				ruta.getDuraccionSegundos());
	}

	private List<Ruta> obtenerRutasDesdeAPI(Vehiculo vehiculo, String acopio, String returng)
			throws IOException, InterruptedException, URISyntaxException {
		EstadoPedido estadoPedido = estadoPedidoRepository.findById(2L).get();
		List<DetallePedido> detallePedidos = detallePedidoRepository.findByEstadoPedidoAndVehiculo(estadoPedido,
				vehiculo);

		List<String> direcciones = detallePedidos.stream()
				.map(detallePedido -> detallePedido.getPedido().getDireccion()).collect(Collectors.toList());

		String requestBody = crearRequestBody(acopio, direcciones);

		String responseBody = enviarRequest(acopio, returng, requestBody);

		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(responseBody,
				objectMapper.getTypeFactory().constructCollectionType(List.class, Ruta.class));
	}

	private String crearRequestBody(String acopio, List<String> direcciones) throws JsonProcessingException {
		Map<String, Object> bodyMap = new HashMap<>();
		bodyMap.put("acopio", acopio);
		bodyMap.put("direcciones", direcciones);

		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(bodyMap);
	}

	private String enviarRequest(String acopio, String returng, String requestBody)
			throws IOException, InterruptedException, URISyntaxException {
		HttpClient client = HttpClient.newHttpClient();
		HttpRequest request = HttpRequest.newBuilder()
				.uri(new URI(
						"http://localhost:8080/ordenEntrega?return=" + returng + "&duration=false&acopio=" + acopio))
				.header("apiKey", "AIzaSyCPdiPlv2soNfunlvvSV4x_nYWahWvXp1o").header("Content-Type", "application/json")
				.POST(HttpRequest.BodyPublishers.ofString(requestBody)).build();

		HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
		return response.body();
	}

	private List<PedidoRs> generarPedidosOrdenadosEntrega(Ruta ruta) {
		List<String> ordenEntrega = ruta.getOrdenEntrega();
		ordenEntrega.remove(Constants.ACOPIO_DIRECCION);
		List<PedidoRs> pedidosOrdenadosEntrega = new ArrayList<>();

		for (String entrega : ordenEntrega) {
			Pedido pedido = pedidoRepository.findByDireccion(entrega).get(0);
			DetallePedido detallePedido = detallePedidoRepository.findByPedido(pedido).get(0);
			pedidosOrdenadosEntrega.add(
					new PedidoRs(pedido.getId(), pedido.getDireccion(), new Date(pedido.getFechaEntrega().getTime()),
							pedido.getPesoGramos(), detallePedido.getEstadoPedido().getNombre()));
		}
		return pedidosOrdenadosEntrega;
	}

	private void guardarRutaEnCache(RutaRs rutaRs, MejorRutaCacheKey key) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		String jsonRuta = mapper.writeValueAsString(rutaRs);
		MejorRutaCache mejorRutaCache = new MejorRutaCache(key, jsonRuta);
		mejorRutaCacheRepository.save(mejorRutaCache);
	}

	private void manejarPedidosConPrioridad(List<EstadoPedido> estadoPedidos) {
		List<VehiculoRs> vehiculosDisponibles = obtenerVehiculosDisponibles();
		List<DetallePedido> detallePedidos = obtenerDetallePedidosOrdenados();
		List<DetallePedido> detallePedidosHoy = obtenerDetallePedidosHoy(detallePedidos);
		List<DetalleVehiculo> detalleVehiculos = obtenerDetalleVehiculos(vehiculosDisponibles);
		Map<String, List<DetallePedido>> detallesPorCodigoPostal = obtenerDetallesPorCodigoPostal(detallePedidosHoy);
		List<Region> regiones = obtenerRegiones(detallesPorCodigoPostal);

		List<DetallePedido> detallePedidosDespachados = distribuirPedidos(regiones, detalleVehiculos,
				new ArrayList<>());
		actualizarEstadoPedidosDespachados(detallePedidosDespachados, estadoPedidos);
		guardarDetallesPedidosDespachados(detallePedidosDespachados);
	}

	private void manejarPedidosSinPrioridad(List<EstadoPedido> estadoPedidos) {
		Optional<EstadoPedido> estadoPedidoOptional = estadoPedidoRepository.findById(ID_ESTADO_PEDIDO_EN_ACOPIO);
		if (estadoPedidoOptional.isPresent()) {
			EstadoPedido estadoPedido = estadoPedidoOptional.get();
			List<DetallePedido> detallePedidosSinEnviar = detallePedidoRepository.findByEstadoPedido(estadoPedido);
			Map<String, List<DetallePedido>> detallesPorCodigoPostalSinPrioridad = obtenerDetallesPorCodigoPostal(
					detallePedidosSinEnviar);
			List<Region> regionesSinPrioridad = obtenerRegiones(detallesPorCodigoPostalSinPrioridad);
			List<VehiculoRs> vehiculosDisponiblesSinPrioridad = obtenerVehiculosDisponibles();
			List<DetalleVehiculo> detalleVehiculosSinPrioridad = obtenerDetalleVehiculos(
					vehiculosDisponiblesSinPrioridad);

			List<DetallePedido> detallePedidosDespachados = distribuirPedidos(regionesSinPrioridad,
					detalleVehiculosSinPrioridad, new ArrayList<>());
			System.out.println(detallePedidosDespachados);

			actualizarEstadoPedidosDespachados(detallePedidosDespachados, estadoPedidos);
			guardarDetallesPedidosDespachados(detallePedidosDespachados);
		} else {
			// Manejar el caso cuando el Optional está vacío
		}
	}

	private void guardarDetallesPedidosDespachados(List<DetallePedido> detallePedidosDespachados) {
		detallePedidoRepository.saveAll(detallePedidosDespachados);
	}

	private List<VehiculoRs> obtenerVehiculosDisponibles() {
		return ((List<VehiculoRs>) obtenerVehiculos()).stream()
				.filter(vehiculo -> vehiculo.getActivo() && vehiculo.getCapacidadDisponibleGramos() != 0)
				.sorted(Comparator.comparing(VehiculoRs::getCapacidadDisponibleGramos).reversed())
				.collect(Collectors.toList());
	}

	private List<DetallePedido> obtenerDetallePedidosOrdenados() {
		Optional<EstadoPedido> estadoPedidoOptional = estadoPedidoRepository.findById(ID_ESTADO_PEDIDO_EN_ACOPIO);
		return estadoPedidoOptional.map(estadoPedido -> detallePedidoRepository.findByEstadoPedido(estadoPedido)
				.stream().sorted(Comparator.comparing(detallePedido -> detallePedido.getPedido().getFechaEntrega()))
				.collect(Collectors.toList())).orElseThrow(); // Lanzar excepción personalizada.
	}

	private List<DetallePedido> obtenerDetallePedidosHoy(List<DetallePedido> detallePedidos) {
		LocalDateTime ultimoMomentoDia = LocalDateTime.of(LocalDateTime.now().toLocalDate(), LocalTime.MAX);
		return detallePedidos.stream().filter(detallePedido -> {
			LocalDateTime fechaEntrega = detallePedido.getPedido().getFechaEntrega().toInstant()
					.atZone(ZoneId.systemDefault()).toLocalDateTime();
			return fechaEntrega.isBefore(ultimoMomentoDia);
		}).collect(Collectors.toList());
	}

	private Map<String, List<DetallePedido>> obtenerDetallesPorCodigoPostal(List<DetallePedido> detallePedidosHoy) {
		return detallePedidosHoy.stream()
				.collect(Collectors.groupingBy(detallePedido -> detallePedido.getPedido().getCodigoPostal()));
	}

	private List<Region> obtenerRegiones(Map<String, List<DetallePedido>> detallesPorCodigoPostal) {
		return detallesPorCodigoPostal.entrySet().stream().map(entry -> {
			return crearRegion(entry);
		}).sorted(Comparator.comparing(Region::getCodigoPostal)).collect(Collectors.toList());
	}

	private Region crearRegion(Map.Entry<String, List<DetallePedido>> entry) {
		String codigoPostal = entry.getKey();
		List<DetallePedido> detalles = entry.getValue().stream()
				.sorted(Comparator.comparing(detalle -> detalle.getPedido().getPesoGramos(), Comparator.reverseOrder()))
				.collect(Collectors.toList());
		double pesoTotal = detalles.stream().mapToDouble(detallePedido -> detallePedido.getPedido().getPesoGramos())
				.sum();
		return new Region(codigoPostal, detalles, pesoTotal);
	}

	private List<DetalleVehiculo> obtenerDetalleVehiculos(List<VehiculoRs> vehiculosDisponibles) {
		return vehiculosDisponibles.stream().map(vehiculo -> {
			return new DetalleVehiculo(vehiculo, new ArrayList<>());
		}).collect(Collectors.toList());
	}

	private List<DetallePedido> distribuirPedidos(List<Region> regiones, List<DetalleVehiculo> detalleVehiculos,
			List<DetallePedido> detallePedidosDespachados) {
		int indiceRegion = 0;

		while (!regiones.isEmpty()) {
			for (DetalleVehiculo detalleVehiculo : detalleVehiculos) {
				if (regiones.isEmpty()) {
					break;
				}

				Region region = regiones.get(indiceRegion);
				VehiculoRs vehiculo = detalleVehiculo.getVehiculo();

				if (vehiculo.getCapacidadDisponibleGramos() >= region.getPesoTotal() && region.getPesoTotal() != 0) {
					descontarPesoYAsignarPedidos(region, vehiculo, detalleVehiculo);
					regiones.remove(indiceRegion);
				} else if (dividir(
						detalleVehiculos.stream().map(DetalleVehiculo::getVehiculo).collect(Collectors.toList()),
						region.getPesoTotal())) {
					if (region.getDetallePedidos().size() != 1) {

						regiones.addAll(dividirRegion(region));
					}
					regiones.remove(indiceRegion);
				}
				detallePedidosDespachados.addAll(detalleVehiculo.getDetallePedidos());
			}
		}

		return detallePedidosDespachados;
	}

	private void descontarPesoYAsignarPedidos(Region region, VehiculoRs vehiculo, DetalleVehiculo detalleVehiculo) {
		vehiculo.setCapacidadDisponibleGramos(vehiculo.getCapacidadDisponibleGramos() - region.getPesoTotal());
		List<DetallePedido> detallePedidosRegion = region.getDetallePedidos();

		for (DetallePedido detallePedidoRegion : detallePedidosRegion) {
			detallePedidoRegion.setVehiculo(new Vehiculo(vehiculo.getId(), vehiculo.getPlaca(),
					vehiculo.getCapacidadGramos(), vehiculo.getActivo()));
		}
		detalleVehiculo.getDetallePedidos().addAll(detallePedidosRegion);
	}

	private List<Region> dividirRegion(Region region) {
		List<DetallePedido> detallePedidosRegion = region.getDetallePedidos();

		int size = region.getDetallePedidos().size();
		List<Region> nuevasRegiones = new ArrayList<>();
		if (size <= 1) {
			nuevasRegiones.add(region);
			return nuevasRegiones;
		}
		List<DetallePedido> detallePedido1 = new ArrayList<>(detallePedidosRegion.subList(0, (size + 1) / 2));
		List<DetallePedido> detallePedido2 = new ArrayList<>(detallePedidosRegion.subList((size + 1) / 2, size));

		double pesoTotal1 = detallePedido1.stream()
				.mapToDouble(detallePedido -> detallePedido.getPedido().getPesoGramos()).sum();

		double pesoTotal2 = detallePedido2.stream()
				.mapToDouble(detallePedido -> detallePedido.getPedido().getPesoGramos()).sum();

		String codigoPostalA = region.getCodigoPostal() + "a";
		String codigoPostalB = region.getCodigoPostal() + "b";

		nuevasRegiones.add(new Region(codigoPostalA, detallePedido1, pesoTotal1));
		nuevasRegiones.add(new Region(codigoPostalB, detallePedido2, pesoTotal2));

		return nuevasRegiones;
	}

	private void actualizarEstadoPedidosDespachados(List<DetallePedido> detallePedidosDespachados,
			List<EstadoPedido> estadoPedidos) {
		for (DetallePedido detallePedido : detallePedidosDespachados) {
			detallePedido.setEstadoPedido(estadoPedidos.get(INDICE_ESTADO_DESPACHADO));
		}
	}

	private boolean dividir(List<VehiculoRs> vehiculosDisponibles, double pesoTotal) {
		Optional<Double> maxCapacidadDisponible = vehiculosDisponibles.stream()
				.map(VehiculoRs::getCapacidadDisponibleGramos).max(Comparator.naturalOrder());

		return maxCapacidadDisponible.isPresent() && pesoTotal > maxCapacidadDisponible.get();
	}

}
