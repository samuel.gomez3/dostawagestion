package co.com.pragma.services;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import co.com.pragma.dto.MejorRutaCacheRs;
import co.com.pragma.dto.PedidoRq;
import co.com.pragma.dto.PedidoRs;
import co.com.pragma.dto.RutaRs;
import co.com.pragma.dto.VehiculoRq;
import co.com.pragma.dto.VehiculoRs;
import co.com.pragma.entities.Pedido;
import co.com.pragma.entities.Vehiculo;

public interface IGestionService {

	public Vehiculo guardarVehiculo(VehiculoRq vehiculoRq) throws Exception;

	public Vehiculo actualizarVehiculo(VehiculoRq vehiculoRq, long id) throws Exception;

	public List<VehiculoRs> obtenerVehiculos() throws Exception;

	public VehiculoRs obtenerVehiculoPorId(long id) throws Exception;

	public void eliminarVehiculoPorId(long id) throws Exception;

	public Pedido guardarPedido(PedidoRq pedidoRq) throws Exception;

	public Pedido actualizacionPedido(PedidoRq pedidoRq, long id) throws Exception;

	public List<PedidoRs> obtenerPedido() throws Exception;

	public PedidoRs obtenerPedidoPorId(long id) throws Exception;

	public void eliminarPedidoPorId(long id) throws Exception;

	public RutaRs consultarMejorRuta(String placa, String cache, String acopio, String returng)
			throws IOException, InterruptedException, URISyntaxException;

	public void entregarPedido(long idPedido);

	public List<MejorRutaCacheRs> direccionVehiculos();
}
