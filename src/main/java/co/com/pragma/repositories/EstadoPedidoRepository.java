package co.com.pragma.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.pragma.entities.EstadoPedido;

public interface EstadoPedidoRepository extends JpaRepository<EstadoPedido, Long>{

}
