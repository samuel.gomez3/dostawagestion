package co.com.pragma.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.pragma.entities.Vehiculo;

public interface VehiculoRepository extends JpaRepository<Vehiculo, Long>{
	public List<Vehiculo> findByPlaca(String placa);
}
