package co.com.pragma.repositories;


import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.pragma.entities.MejorRutaCache;
import co.com.pragma.entities.MejorRutaCacheKey;

public interface MejorRutaCacheRepository extends JpaRepository<MejorRutaCache, MejorRutaCacheKey> {
	
	public List<MejorRutaCache> findByIdFechaEntrega(Date fechaEntrega);

}
