package co.com.pragma.repositories;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.pragma.entities.Pedido;

public interface PedidoRepository extends JpaRepository<Pedido, Long>{
	
	public List<Pedido> findByDireccion(String direccion);
	

}
