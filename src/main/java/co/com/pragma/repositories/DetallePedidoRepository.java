package co.com.pragma.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.pragma.entities.DetallePedido;
import co.com.pragma.entities.EstadoPedido;
import co.com.pragma.entities.Pedido;
import co.com.pragma.entities.Vehiculo;

public interface DetallePedidoRepository extends JpaRepository<DetallePedido, Long>{
	
	
	public List<DetallePedido> findByVehiculo(Vehiculo vehiculo);
	public List<DetallePedido> findByPedido(Pedido pedido);
	public List<DetallePedido> findByEstadoPedido(EstadoPedido EstadoPedido);
	public List<DetallePedido> findByEstadoPedidoAndVehiculo(EstadoPedido EstadoPedido, Vehiculo vehiculo);
}
