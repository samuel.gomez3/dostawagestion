package co.com.pragma.util;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class GestionUtil {
	

	
    public static String obtenerCodigoPostal(String direccion) {
    	
    	RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("apiKey", "AIzaSyCPdiPlv2soNfunlvvSV4x_nYWahWvXp1o");

        Map<String, List<String>> body = Collections.singletonMap("direcciones", Collections.singletonList(direccion));

        HttpEntity<Map<String, List<String>>> entity = new HttpEntity<>(body, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                "http://localhost:8080/codigoPostal",
                HttpMethod.POST,
                entity,
                String.class);

        return response.getBody();
    }

}
