package co.com.pragma.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;

@Service
public class JwtUtil {

	private static String SECRET_KEY = "SamI1752";

	public static String generateToken(String username, Map<String, Object> claims) {
		return Jwts.builder().setClaims(claims).setSubject(username).setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 600000)) // 10 minutes
				.signWith(SignatureAlgorithm.HS256, SECRET_KEY).compact();
	}

	public static Claims validateToken(String token) {
		Claims claims = Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody();

		return claims;
	}

}
